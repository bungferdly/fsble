//
//  ViewController.m
//  FSBLE
//
//  Created by Ferdly Sethio on 10/9/14.
//  Copyright (c) 2014 Ferdly Sethio. All rights reserved.
//

#import "ViewController.h"
#import "FSBLE.h"

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	NSDictionary *dict = @{
						   @"birthday" : @"12/03/1988",
						   @"first_name" : @"Ferdly",
						   @"friends" : @[
								   @"10202966782553658",
								   @"1476588389266773",
								   ],
						   @"gender" : @"male",
						   @"likes" : @[
								   @"329509440534509",
								   @"17353926534",
								   @"1430838427172445",
								   @"23050342417",
								   @"226576327481866",
								   @"227227327288171",
								   @"275904399136748",
								   @"114104825312708",
								   @"151995825386",
								   @"165749551714",
								   @"49283544849",
								   @"106357469967",
								   @"32801197536",
								   @"111288175596355",
								   @"202471215497",
								   @"114548361889913",
								   @"10045457582",
								   @"112421635436437",
								   @"108775869153628",
								   @"103091413063976",
								   @"42798291365",
								   @"32801197536",
								   @"111288175596355",
								   @"202471215497",
								   @"114548361889913",
								   @"10045457582",
								   @"112421635436437",
								   @"108775869153628",
								   @"103091413063976"
								   ],
						   @"objectId" : @"YTbRdP4Nkv",
						   @"photos" : @[
								   @"https://fbcdn-sphotos-b-a.akamaihd.net/hphotos-ak-xfp1/v/t1.0-9/10468374_10204661054753015_4917455830610317070_n.jpg?oh=f92b29bc58b9a63a7ecfcc6dcba80f02&oe=54C1243D&__gda__=1421963608_a112a6eea609b8a3a1e7600e7a31405c",
								   @"https://fbcdn-sphotos-c-a.akamaihd.net/hphotos-ak-xfa1/v/t1.0-9/10487457_10204634800656679_4286918381090799365_n.jpg?oh=86df06d03fc074f42e4600370fd746f3&oe=54C9076D&__gda__=1422690176_25e688b3e6694e8b51d91fd756e579cf",
								   @"https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-xpf1/v/t1.0-9/1907672_10204408374916177_6830678358199042354_n.jpg?oh=371a42796e90535b1ab8f83e8760498b&oe=54C0E785&__gda__=1420671418_dfb3be251f8e5e4a99c37eec5a2c9589",
								   @"https://fbcdn-sphotos-b-a.akamaihd.net/hphotos-ak-xpf1/v/t1.0-9/1004540_10202412632063853_1247003263_n.jpg?oh=0c3fda89e03333726c34be6649ed809e&oe=54B24F82&__gda__=1421488479_63d2f35e44ebf1ca5026732a87b65a42",
								   @"https://fbcdn-sphotos-g-a.akamaihd.net/hphotos-ak-xaf1/v/t1.0-9/303829_10200358677396270_772366961_n.jpg?oh=f832ea390add77e95ab8d653377da1b6&oe=54F6BE17&__gda__=1420942020_c42e3e85e01fd1223ac4a4dd3d29bd86",
								   @"https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-xfa1/v/t1.0-9/s720x720/379407_3066067057392_1834724579_n.jpg?oh=66739503348742a045386d8c40fce07d&oe=54C7427C&__gda__=1421950615_23af4bdd6365e576ff20de2b9af628b6"
								   ],
						   @"sound" : @1,
						   @"status" : @"",
						   @"userId" : @"10204699040862644"
						   };
	
	[FSBLE instance].advertiseData = [NSKeyedArchiver archivedDataWithRootObject:dict];
	[FSBLE instance].didScanDataBlock = ^(NSData *data){
		NSLog(@"%@",[NSKeyedUnarchiver unarchiveObjectWithData:data]);
	};
	[[FSBLE instance] startAdvertising];
	[[FSBLE instance] startScanning];
}

@end
