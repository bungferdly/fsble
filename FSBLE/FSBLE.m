//
//  FSBLE.m
//  FSBLE
//
//  Created by Ferdly Sethio on 10/9/14.
//  Copyright (c) 2014 Ferdly Sethio. All rights reserved.
//

#import "FSBLE.h"
#import <CoreBluetooth/CoreBluetooth.h>

#define DLog(fmt, ...) NSLog((@"%s " fmt), __PRETTY_FUNCTION__, ##__VA_ARGS__)

#define START_SCANNING_IF_ERROR \
	DLog(@"Error : %@", error);\
	if (error) {\
		[self startScanning];\
		return;\
	}

@interface FSBLE()<CBPeripheralManagerDelegate, CBCentralManagerDelegate, CBPeripheralDelegate>{
	dispatch_queue_t queue;
	CBUUID *serviceUUID;
	CBUUID *characteristicUUID;
	CBMutableCharacteristic *characteristic;
	NSTimer *timer;
	
	CBPeripheralManager * peripheralManager;
	NSMutableArray *centrals;
	NSUInteger sendIndex;
	NSUInteger sendDataLength;
	
	CBCentralManager *centralManager;
	CBPeripheral *currentPeripheral;
	NSMutableData *currentData;
	NSMutableArray *scannedIDs;
}

@end

@implementation FSBLE

+(instancetype)instance{
	static FSBLE *_fsble = nil;
	if (!_fsble){
		_fsble = [[FSBLE alloc] init];
	}
	return _fsble;
}

- (instancetype)init{
	self = [super init];
	queue = dispatch_queue_create("com.bungferdly.FSBLE", DISPATCH_QUEUE_SERIAL);
	serviceUUID = [CBUUID UUIDWithString:FSBLE_DEFAULT_SERVICEID];
	characteristicUUID = [CBUUID UUIDWithString:FSBLE_DEFAULT_CHARACTERISTICID];
	characteristic = [[CBMutableCharacteristic alloc] initWithType:characteristicUUID
														properties:CBCharacteristicPropertyNotify
															value:nil
														permissions:CBAttributePermissionsReadable];
	scannedIDs = [NSMutableArray array];
	centrals = [NSMutableArray array];
	return self;
}

- (void)startAdvertising{
	if (!peripheralManager){
		peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self queue:queue];
		
	}else if (peripheralManager.state == CBPeripheralManagerStatePoweredOn){
		
		sendIndex = 0;
		[centrals removeAllObjects];
		
		DLog(@"App State : %d", [[UIApplication sharedApplication] applicationState]);
		
		CBMutableService *service = [[CBMutableService alloc] initWithType:serviceUUID primary:YES];
		service.characteristics = @[characteristic];
		[peripheralManager removeAllServices];
		[peripheralManager addService:service];
		[peripheralManager startAdvertising:@{CBAdvertisementDataServiceUUIDsKey : @[serviceUUID]}];
	}
}

- (void)stop{
	DLog();
	[peripheralManager stopAdvertising];
	[self stopScanning];
	peripheralManager = nil;
	centralManager = nil;
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral{
	[self performSelectorOnMainThread:@selector(startAdvertising) withObject:nil waitUntilDone:NO];
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral central:(CBCentral *)central didSubscribeToCharacteristic:(CBCharacteristic *)c{
	DLog();
	if ([c.UUID.UUIDString isEqualToString:characteristicUUID.UUIDString]) {
		[centrals addObject:central];
		if (centrals.count == 1) {
			[self beginSendDataToFirstCentral];
		}
	}
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral central:(CBCentral *)central didUnsubscribeFromCharacteristic:(CBCharacteristic *)c{
	DLog(@"%lu", (unsigned long)sendIndex);
	
	[centrals removeObject:central];
	[self beginSendDataToFirstCentral];
}

- (void)beginSendDataToFirstCentral{
	sendIndex = 0;
	if (centrals.count > 0) {
		sendDataLength = [centrals[0] maximumUpdateValueLength];
		[self beginSendData];
	}else{
		[self startScanning];
	}
}

- (void)beginSendData{
	while(centrals.count>0){
		NSData *chunk = nil;
		
		if(sendIndex >= _advertiseData.length){
			chunk = [NSData data];
		}else{
			NSInteger amountToSend = MIN(_advertiseData.length - sendIndex, sendDataLength);
			chunk = [_advertiseData subdataWithRange:NSMakeRange(sendIndex, amountToSend)];
		}
		
		if (![peripheralManager updateValue:chunk forCharacteristic:characteristic onSubscribedCentrals:@[centrals[0]]]) {
			break;
		}

		if (chunk.length != 0){
			DLog(@"%lu", (unsigned long)chunk.length);
			sendIndex += chunk.length;
		}else{
			DLog("End sending data");
			return;
		}
	}
}

- (void)peripheralManagerIsReadyToUpdateSubscribers:(CBPeripheralManager *)peripheral{
	[self beginSendData];
}
















//-------------------

- (void)cleanUp{
	[scannedIDs removeAllObjects];
	[self startScanning];
}

- (void)startScanning{
	if ([NSThread currentThread] != [NSThread mainThread]) {
		[self performSelectorOnMainThread:@selector(startScanning) withObject:nil waitUntilDone:NO];
		return;
	}
	
	if(!centralManager){
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cleanUp) name:UIApplicationDidEnterBackgroundNotification object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cleanUp) name:UIApplicationWillEnterForegroundNotification object:nil];
		centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:queue];
		
	}else if(centralManager.state == CBCentralManagerStatePoweredOn){
		
		if (currentPeripheral) {
			[centralManager cancelPeripheralConnection:currentPeripheral];
			currentPeripheral = nil;
		}
		
		_isScanning = YES;
		if (_isScanningBlock){
			_isScanningBlock();
		}
		
		[timer invalidate];
		timer = nil;
		currentData = [NSMutableData data];
		
		DLog(@"App State : %d", [[UIApplication sharedApplication] applicationState]);
		[centralManager scanForPeripheralsWithServices:@[serviceUUID] options:@{CBCentralManagerScanOptionAllowDuplicatesKey : @YES}];
		
		dispatch_async(queue, ^{
			dispatch_sync(dispatch_get_main_queue(), ^{
				if ([UIApplication sharedApplication].applicationState != UIApplicationStateBackground){
					timer = [NSTimer scheduledTimerWithTimeInterval:FSBLE_SCANTIME target:self selector:@selector(rescheduleScanning) userInfo:nil repeats:NO];
				}
			});
		});
	}
}

- (void)stopScanning{
	if ([NSThread currentThread] != [NSThread mainThread]) {
		[self performSelectorOnMainThread:@selector(stopScanning) withObject:nil waitUntilDone:NO];
		return;
	}
	
	DLog();
	[centralManager stopScan];
	[timer invalidate];
	timer = nil;
}

- (void)rescheduleScanning{
	[self stopScanning];
	timer = [NSTimer scheduledTimerWithTimeInterval:FSBLE_SCANINTERVAL target:self selector:@selector(startScanning) userInfo:nil repeats:NO];
	
	_isScanning = NO;
	if (_isScanningBlock){
		_isScanningBlock();
	}
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central{
	[self startScanning];
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI{
	
	DLog(@"App State : %d", [[UIApplication sharedApplication] applicationState]);
	
	NSString *pID = peripheral.identifier.UUIDString;
	if (currentPeripheral || [scannedIDs indexOfObject:pID] != NSNotFound){
		return;
	}
	
	[self stopScanning];
	currentPeripheral = peripheral;
	[centralManager connectPeripheral:peripheral options:nil];
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral{
	DLog();
	peripheral.delegate = self;
	[peripheral discoverServices:@[serviceUUID]];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
	DLog(@"Error : %@", error);
	[self startScanning];
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
	DLog(@"Error : %@", error);
	[self startScanning];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error{
	
	START_SCANNING_IF_ERROR;
	
	for (CBService *service in peripheral.services) {
		if ([service.UUID.UUIDString isEqualToString:FSBLE_DEFAULT_SERVICEID]){
			[peripheral discoverCharacteristics:@[characteristicUUID] forService:service];
			break;
		}
	}
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error{
	
	START_SCANNING_IF_ERROR;
	
	for (CBCharacteristic *c in service.characteristics) {
		if ([c.UUID.UUIDString isEqualToString:FSBLE_DEFAULT_CHARACTERISTICID]){
			[peripheral setNotifyValue:YES forCharacteristic:c];
			break;
		}
	}
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)c error:(NSError *)error{
	
	START_SCANNING_IF_ERROR;
	
	if (c.value.length == 0) {
		DLog("End receiving data");
		[peripheral setNotifyValue:NO forCharacteristic:c];
		[scannedIDs addObject:peripheral.identifier.UUIDString];
		if (_didScanDataBlock) {
			dispatch_sync(dispatch_get_main_queue(), ^{
				_didScanDataBlock(currentData);
			});
		}
		[self startScanning];
		return;
	}
	
	[currentData appendData:c.value];
}

@end
