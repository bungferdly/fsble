//
//  AppDelegate.h
//  FSBLE
//
//  Created by Ferdly Sethio on 10/9/14.
//  Copyright (c) 2014 Ferdly Sethio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

