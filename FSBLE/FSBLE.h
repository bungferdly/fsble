//
//  FSBLE.h
//  FSBLE
//
//  Created by Ferdly Sethio on 10/9/14.
//  Copyright (c) 2014 Ferdly Sethio. All rights reserved.
//

#import <UIKit/UIKit.h>

#define FSBLE_DEFAULT_SERVICEID @"FB694B90-F49E-4597-8306-171BBA78F849"
#define FSBLE_DEFAULT_CHARACTERISTICID @"EB6727C4-F184-497A-A656-76B0CDAC6333"
#define FSBLE_SCANTIME 5
#define FSBLE_SCANINTERVAL 15

typedef void (^FSBLEDidScanDataBlock) (NSData *data);
typedef void (^FSBLEIsScanningBlock) ();

@interface FSBLE : NSObject

@property(assign, nonatomic)BOOL isScanning;
@property(strong, nonatomic)NSData *advertiseData;
@property(strong, nonatomic)FSBLEDidScanDataBlock didScanDataBlock;
@property(strong, nonatomic)FSBLEIsScanningBlock isScanningBlock;

+(instancetype)instance;

- (void)startAdvertising;
- (void)startScanning;
- (void)stop;

@end
